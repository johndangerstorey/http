'use strict';

angular.module('HttpLeagueApp', [
	//'ngRoute'
])
// .config(function($interpolateProvider){
//     $interpolateProvider.startSymbol('%$').endSymbol('$%');
// });
// .config(['$routeProvider', function($routeProvider) {
//   $routeProvider.otherwise({ redirectTo: '/' });
// }]);

// controller
angular.module('HttpLeagueApp').controller('RankController',
	[ '$scope'
	, '$window'
	, '$http'
	, function ($scope, $window, $http) {
		var RC = this;
		
		$http.get('/api/http-league/users').then(function(res){
			RC.users = res.data;
			console.log(RC.users);
		});

		$http.get('/api/http-league/session').then(function(res){
			RC.session = res.data;
			if(RC.session.accounts.length > 1 ){
				throw new Error('user switching not implemented');
			}
			RC.user = RC.session.accounts[0];
			//TODO: use the account that the user chooses (ask AJ)
		});

		RC.submit = function(match){
			match._csrf = window._csrf;
			console.log(match);
			$http.post('/api/http-league/matches/' + match.opponentId, match).then(function(res){
				//
				var user;
				var opponent;
				var usersMap = {};
				RC.users.forEach(function(user){
					usersMap[user._id] = user;
				});


				user = usersMap[res.data.user.id];
				opponent = usersMap[res.data.opponent.id];

				console.log('user', JSON.stringify(user, null, '  '));
				console.log('opponent', JSON.stringify(opponent, null, '  '));

				user.profile.rank = res.data.user.rank;
				user.profile.score = res.data.user.score;
				user.matches.push(res.data.user.match);

				opponent.profile.rank = res.data.opponent.rank;
				opponent.profile.score = res.data.opponent.score;
				opponent.matches.push(res.data.opponent.match);

				console.log('user', JSON.stringify(user, null, '  '));
				console.log('opponent', JSON.stringify(opponent, null, '  '));

				RC.users.sort(function(a,b){
					return a.profile.rank-b.profile.rank;
				});


			});
		};
	}]
);